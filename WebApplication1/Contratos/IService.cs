﻿using Summit.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Contratos
{
    interface IService
    {
        public ServiceState Execute(DataDict dataDict);
    }
}
