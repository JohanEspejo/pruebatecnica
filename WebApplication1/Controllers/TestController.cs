﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Summit.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Class;

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {
        private TestService testService;

        [HttpPost]
        public ActionResult<DataDict> Execute(DataDict dataDict)
        {
            testService = new TestService();
            
            dataDict.MessageError=Convert.ToString( testService.Execute(dataDict));
            return Ok(dataDict);
        }
    }
}
