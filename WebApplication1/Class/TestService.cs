﻿using Summit.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using WebApplication1.Contratos;

namespace WebApplication1.Class
{
    public class TestService : IService
    {

        private ServiceState ServiceState;
        public ServiceState Execute(DataDict dataDict)
        {
            
            if (dataDict.MessageError == null & dataDict.Text==null)
            {
                dataDict.MessageError = "No hay datos";
                return ServiceState.Aborted;
            }
            else if(string.IsNullOrEmpty(dataDict.Text))
            {
                dataDict.MessageError = "No hay Texto";
                return ServiceState.Rejected;
            }

            if (Regex.IsMatch(dataDict.Text, @"^[0-9]+$"))
            {
                NumericService numericService = new NumericService(dataDict);
                numericService.StartService();
            }
            else
            {
                AlphaNumericService alphaNumericService = new AlphaNumericService(dataDict);
                alphaNumericService.StartService();
            }

            return ServiceState;
        }
    }
}
