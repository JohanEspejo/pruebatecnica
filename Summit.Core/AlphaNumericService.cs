﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Summit.Core
{
    public class AlphaNumericService : DataService
    {
        private DataDict DataDict;
        public AlphaNumericService(DataDict dataDict) : base(dataDict)
        {
            this.DataDict = dataDict;
        }
        protected override ServiceState Process()
        {
            int vocales = 0;
            string texto = DataDict.Text.ToLower();
            for (int i = 0; i < texto.Length; i++)
            {
                switch (texto[i])
                {
                    case 'a':
                        vocales += 1;
                        break;
                    case 'e':
                        vocales += 1;
                        break;
                    case 'i':
                        vocales += 1;
                        break;
                    case 'o':
                        vocales += 1;
                        break;
                    case 'u':
                        vocales += 1;
                        break;
                }
            }

            DataDict.Text = "El texto es alfanumérico y el total de vocales es " + vocales;

            return ServiceState.Accepted;
        }
    }
}
