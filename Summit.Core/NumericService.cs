﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Summit.Core
{
    public class NumericService : DataService
    {
        private DataDict DataDict;
        public NumericService(DataDict dataDict) : base(dataDict)
        {
            DataDict = dataDict;
        }

        protected override ServiceState Process()
        {
            int numero = DataDict.Text.Length;
            if(numero<=4)
            {
                string respuesta = "El texto es numérico y el número enmascarcado es:" + DataDict.Text;
                DataDict.Text = respuesta;
                return ServiceState.Accepted;
            }
            else
            {
                string result= DataDict.Text.Substring(DataDict.Text.Length - 4).PadLeft(DataDict.Text.Length, '#');
                string respuesta = "El texto es numérico y el número enmascarcado es:" + result;
                DataDict.Text = respuesta;

                return ServiceState.Accepted;
            }
          
           
        }
    }
}
