﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Summit.Core
{
    public enum ServiceState
    {
        Accepted,
        Rejected,
        Aborted

    }
}
