﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Summit.Core
{
    public abstract class ServiceBase
    {
        public DataDict Dictionary;

        public  ServiceBase(DataDict dataDict)
        {
            this.Dictionary = dataDict;
        }

        public abstract ServiceState StartService();
       
    }
}
